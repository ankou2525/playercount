package com.gmail.ankou2525;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class Main extends JavaPlugin implements Listener{
	List<Player> join = new ArrayList<Player>();
	String pf = ChatColor.GREEN+"[MG] "+ChatColor.RESET;
	private boolean pc;

	Logger log;

	public void onEnable(){
		log = this.getLogger();
		this.getServer().getPluginManager().registerEvents(this, this);
		log.info("playercount is Enbale!");
	}
	public void onDisable(){
		log.info("playercount is Disable!");
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		if(join.contains(p)){
			join.remove(p);
		}
	}
	@EventHandler
	public void onPJ(PlayerJoinEvent e){
		Player p = e.getPlayer();
		p.teleport(p.getWorld().getSpawnLocation());
	}
	@EventHandler
	public void onPD(PlayerDeathEvent e){
		Player p = e.getEntity();
		if(join.contains(p)){
			if(this.pc){
				join.remove(p);
				Bukkit.broadcastMessage(pf + ChatColor.AQUA +"残り人数"+ChatColor.YELLOW + join.size() + ChatColor.AQUA +"人です！");
			}
			if(this.pc){
				if(join.size() == 1){
					ScoreboardManager manager = Bukkit.getScoreboardManager();
					Scoreboard board = manager.getMainScoreboard();
					Team t = board.getTeam("mvp");
					Player win = join.get(0).getPlayer();
					t.addPlayer(win);
					Bukkit.broadcastMessage(pf + ChatColor.AQUA +"試合終了！");
					Bukkit.broadcastMessage(pf + ChatColor.AQUA+""+ ChatColor.BOLD +"*==============*");
					Bukkit.broadcastMessage(pf + ChatColor.AQUA+""+ ChatColor.BOLD +"優勝:"+ ChatColor.GOLD +""+ win.getName() +"");
					Bukkit.broadcastMessage(pf + ChatColor.AQUA+""+ ChatColor.BOLD +"*==============*");
					for(Player tg: Bukkit.getOnlinePlayers()){
						PlayerInventory pi = tg.getInventory();
						pi.clear();
						join.remove(tg);
						this.pc = false;
					}
				}
			}
		}
	}
	public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args){
		if(cmd.getName().equalsIgnoreCase("pc")){
			if(args[0].equalsIgnoreCase("start")){
				this.pc = true;
				sender.sendMessage(pf + ChatColor.AQUA +"カウントを有効にしました。");
				return false;
			}
			if(args[0].equalsIgnoreCase("player")){
				if(args[1].equalsIgnoreCase("reset")){
					join.clear();
					sender.sendMessage(pf + ChatColor.AQUA +"カウント人数をリセットしました。");
					this.pc = false;
					return false;
				}
				if(args[1].equalsIgnoreCase("set")){
					for(Player tg: Bukkit.getOnlinePlayers()){
						join.add(tg);
					}
					sender.sendMessage(pf + ChatColor.AQUA +"カウント人数を"+ChatColor.YELLOW + join.size() + ChatColor.AQUA +"人に設定しました。");
					return false;
				}
			}
		}
		return false;
	}
}
